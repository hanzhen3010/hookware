package main

import (
  "encoding/json"
  "fmt"
  "log"
  "net/http"
)

type Context struct {
  Events []string
  Tokens []string
}

type validator struct {
  events StringSet
  tokens StringSet
}

func (ctx Context) Validator() validator {
  return validator{
    events: NewStringSet(ctx.Events...),
    tokens: NewStringSet(ctx.Tokens...),
  }
}

func (v validator) ValidateRequest(r *http.Request) bool {
  if r.Method != "POST" {
    log.Printf("Request failed validation: request method is %s\n", r.Method)
    return false
  }
  if v.tokens != nil && len(v.tokens) > 0 {
    if !v.tokens.Contains(r.Header.Get("X-Gitlab-Token")) {
      log.Println("Request failed validation: secret token did not match")
      return false
    }
  }
  if v.events != nil && len(v.events) > 0 {
    if !v.events.Contains(r.Header.Get("X-Gitlab-Event")) {
      log.Printf("Request failed validation: event did not match, event header = %s\n", r.Header.Get("X-Gitlab-Event"))
      return false
    }
  }
  return true
}

func HandleHook(notify chan<- map[string]interface{}, ctx Context) func(w http.ResponseWriter, r *http.Request) {
  v := ctx.Validator()
  return func(w http.ResponseWriter, r *http.Request) {
    if v.ValidateRequest(r) {
      decoder := json.NewDecoder(r.Body)
      var event map[string]interface{}
      err := decoder.Decode(&event)
      defer r.Body.Close()

      if err != nil {
        log.Printf("Error decoding JSON payload: %s\n", err)
      } else {
        go func() {
          notify <- event
        }()
      }
    }
    fmt.Fprintf(w, "ok\n")
  }
}
