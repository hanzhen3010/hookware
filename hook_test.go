package main

import (
  "testing"
  "io/ioutil"
  "net/http"
  "strings"
  "time"
)

func expectEvent(ec <-chan map[string]interface{}, t *testing.T) {
  select {
  case event := <-ec:
    t.Logf("Received event: %+v\n", event)
  case <-time.After(time.Millisecond * 25):
    t.Fatal("Event not received within timeout")
  }
}

func expectNoEvent(ec <-chan map[string]interface{}, t *testing.T) {
  select {
  case event := <-ec:
    t.Fatal("Unexpected event received: %+v\n", event)
  case <-time.After(time.Millisecond * 25):
  }
}

func TestHook(t *testing.T) {
  ec := make(chan map[string]interface{})
  handleFunc := HandleHook(ec, Context{})

  req := &http.Request{
    Method: "POST",
    Body: ioutil.NopCloser(strings.NewReader(`
      {
        "created_at": "2012-07-21T07:30:54Z",
        "updated_at": "2012-07-21T07:38:22Z",
        "event_name": "project_create"
      }`)),
    Header: make(map[string][]string),
  }
  req.Header.Add("X-Gitlab-Event", "System Event")
  rw := NewMockResponseWriter()
  handleFunc(rw, req)

  defer expectEvent(ec, t)

  if rw.Buffer.String() == "" {
    t.Fatal("Empty response body")
  }
}

func TestHookRejectsGetMethod(t *testing.T) {
  ec := make(chan map[string]interface{})
  handleFunc := HandleHook(ec, Context{})

  req := &http.Request{
    Method: "GET",
    Body: ioutil.NopCloser(strings.NewReader("{}")),
  }

  rw := NewMockResponseWriter()
  handleFunc(rw, req)

  defer expectNoEvent(ec, t)
}

func TestHookRejectsInvalidToken(t *testing.T) {
  ec := make(chan map[string]interface{})
  handleFunc := HandleHook(ec, Context{ Tokens: []string{ "secret" } })

  req := &http.Request{
    Method: "POST",
    Body: ioutil.NopCloser(strings.NewReader("{}")),
    Header: make(map[string][]string),
  }

  req.Header.Add("X-Gitlab-Token", "shh")
  rw := NewMockResponseWriter()
  handleFunc(rw, req)

  defer expectNoEvent(ec, t)
}

func TestHookAcceptsValidToken(t *testing.T) {
  ec := make(chan map[string]interface{})
  handleFunc := HandleHook(ec, Context{ Tokens: []string{ "secret" } })

  req := &http.Request{
    Method: "POST",
    Body: ioutil.NopCloser(strings.NewReader("{}")),
    Header: make(map[string][]string),
  }

  req.Header.Add("X-Gitlab-Token", "secret")
  rw := NewMockResponseWriter()
  handleFunc(rw, req)

  defer expectEvent(ec, t)
}

func TestHookRejectsInvalidEvent(t *testing.T) {
  ec := make(chan map[string]interface{})
  handleFunc := HandleHook(ec, Context{
    Events: []string{"Push Event", "System Event"},
  })

  req := &http.Request{
    Method: "POST",
    Body: ioutil.NopCloser(strings.NewReader("{}")),
    Header: make(map[string][]string),
  }

  req.Header.Add("X-Gitlab-Event", "Tag Push Event")
  rw := NewMockResponseWriter()
  handleFunc(rw, req)

  defer expectNoEvent(ec, t)
}

func TestHookAcceptsValidEvent(t *testing.T) {
  ec := make(chan map[string]interface{})
  handleFunc := HandleHook(ec, Context{
    Events: []string{"Push Event", "System Event"},
  })

  req := &http.Request{
    Method: "POST",
    Body: ioutil.NopCloser(strings.NewReader("{}")),
    Header: make(map[string][]string),
  }

  req.Header.Add("X-Gitlab-Event", "System Event")
  rw := NewMockResponseWriter()
  handleFunc(rw, req)

  defer expectEvent(ec, t)
}
